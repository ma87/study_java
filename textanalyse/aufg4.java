package textanalyse;

import static java.lang.System.out;

public class aufg4 {
	public static void main(String args[]){
		String wort="hallo";
		String[] text={"hallo", "hi", "ciao", "hallo"};
		
		out.println("Das Wort '"+wort+"' kommt "+vielfachheit(wort, text)+" mal vor");
	}
	
	static int vielfachheit(String wort, String[] text){
		int i=0,anzahl=0;
		while (i<text.length){
			if (wort==text[i]) anzahl++;
			i++;
		}
		return anzahl;
		}
}
