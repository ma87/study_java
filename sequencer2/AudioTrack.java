package sequencer2;

public class AudioTrack extends Track {	
	
	public AudioTrack(String name) {
		super(name);
	}
	
	public void play(){
		super.play();
	}
	
	public void play(int vol){
		super.play(vol);		
	}
	
	protected String getName(){
		return getClass().getName().split("\\.")[1];
	}
}
