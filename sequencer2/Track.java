package sequencer2;

abstract public class Track {
	private String name;
	private int vol=63;
	private boolean muted=false;

	public Track(String name){
		this.name=name;
	}
	
	void record(){
		System.out.println("recording " + getName() + " " + name);
	}
	
	
	void play(){
		System.out.println("playing " + getName() + " " + name);
	}
	
	void mute(){
		muted=!muted;
		if (!muted) System.out.print("un");
		System.out.print("muted " + getName() + " " + name +"\n");
	}
	
	void play(int vol){
		if (vol >= 0 && vol <= 127) this.vol=vol;
		System.out.println("playing " + getName() + " " + name + " (Vol "+this.vol+")");
	}
	
	void configure(int vol){
		if (vol >= 0 && vol <= 127) this.vol=vol;
	}
	
	void configure(int vol, int inst){
		System.out.println("error");
	}

	abstract protected String getName();
}
