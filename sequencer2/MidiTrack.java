package sequencer2;

public class MidiTrack extends Track {
	private int instrument=0;
	
	public MidiTrack(String name){
		super(name);
	}
	
	public MidiTrack(String name, int instrument){		
		super(name);
		this.instrument=instrument;
	}
	
	public void play(int vol){
		super.play(vol);
		System.out.println("   Instrument: "+instrument);
	}

	protected String getName(){
		return getClass().getName().split("\\.")[1];
	}
	
	void configure(int vol, int inst){
		super.configure(vol);
		instrument=inst;
	}
}
