package sequencer2;

public class Sequencer {
	private Track[] tracks=new Track[4];
	
	public Sequencer() {
		tracks[0]=new AudioTrack("ummz");
		tracks[1]=new MidiTrack("lalelu");
		tracks[2]=new AudioTrack("blub");
		tracks[3]=new MidiTrack("test");
	}
	
	public void play(){
		tracks[0].play(63);
		tracks[1].play(63);
		tracks[2].play(63);
		tracks[3].play(63);
	}
	
	public void play(int vol){
		tracks[0].play(vol);
		tracks[1].play(vol);
		tracks[2].play(vol);
		tracks[3].play(vol);
	}
			
	public void record(){
		tracks[0].record();
		tracks[1].record();
		tracks[2].record();
		tracks[3].record();
	}
	
	public void mute(int track){
		tracks[track].mute();
	}
	
	public void configure(int track, int vol){
		tracks[track].configure(vol);
	}
	
	public void configure(int track, int vol, int inst){
		tracks[track].configure(vol, inst);
	}
}
