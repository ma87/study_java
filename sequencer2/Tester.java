package sequencer2;

public class Tester {
	public static void main(String[] args) {
		Tester myTest=new Tester();
		myTest.test();
	}
	
	public void test(){
		Sequencer seq=new Sequencer();
		seq.configure(3, 88, 7);
		seq.play(121);
		seq.mute(0);
		seq.mute(0);
		seq.configure(0, 83);
		seq.record();
		seq.play();
	}
}
