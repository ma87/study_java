package figurenEditor2.figuren;

public abstract class Figur {
	public Figur(double x, double y, double h, double b, double rot){
		zentrumX=x;
		zentrumY=y;
		breite=b;
		hoehe=h;
		rotationsWinkel=rot;
	}
	
	protected double zentrumX;
	protected double zentrumY;
	protected double breite;
	protected double hoehe;
	protected double rotationsWinkel;
	
	public String toString(){
		return "zX: " + zentrumX + "\n" +
				"zY: " + zentrumY + "\n" +
				"b: " + breite + "\n" + 
				"h: " + hoehe + "\n" + 
				"Rotationswinkel: " + rotationsWinkel;
	}
	
	public void skalieren(double faktor){
		breite*=faktor;
		hoehe*=faktor;
	}
	
	public double getFlaeche(){
		return breite*hoehe;
	}
	
	public String getName(){
		return "ach leck mich doch";
	}
}
