package schach0;

import java.util.Iterator;

public interface Iterable {
	public Iterator<IFigur> iterator();
}
