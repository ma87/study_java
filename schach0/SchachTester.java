package schach0;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SchachTester {	
	public static void main(String[] args) {
		SchachTester st = new SchachTester();
		st.test();
	}
	
	public void test(){
		Schach s=new Schach();
		System.out.println(s.getFigur("K").getName());
		
		List<String> list = Arrays.asList("K", "D", "T", "L", "S", "B");
		Iterator<String> it = list.iterator();		
		while(it.hasNext()){
			System.out.println(s.getFigur(it.next().toString()).getName());
		}
		
		s.getFigur("D").schlagen("S");
		System.out.println("Springer geschlagen...");
		
//		System.out.println(s.getFigur("S").getName()); //error, weil geschlagen
		
	}

}
