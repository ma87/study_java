package schach0;

import java.util.HashMap;
import java.util.Iterator;

public class Schach {
	public Schach(){
		hm.put("K", new Figur("K�nig"));
		new Figur("K�nig");
		hm.put("D", new Figur("Dame"));
		new Figur("Dame");
		hm.put("T", new Figur("Turm"));
		new Figur("Turm");
		hm.put("L", new Figur("L�ufer"));
		new Figur("L�ufer");
		hm.put("S", new Figur("Springer"));
		new Figur("Springer");
		hm.put("B", new Figur("Bauer"));
		new Figur("Bauer");	
	}
	
	private HashMap<String, IFigur> hm=new HashMap<String, IFigur>();
	
	public IFigur getFigur(String key){
		return hm.get(key);
	}
	/**********/
	private class Figur implements IFigur{
		private Figur(String name){
			n=name;		
		}
		private String n;
		
		public String getName() {
				return n;
		}

		public void schlagen(String key) {
			hm.remove(key);			
		}			
	}
	
	public Iterator<IFigur> iterator() {
		return hm.values().iterator();
	}
}
