package schach0;

public interface IFigur {

	public String getName();
	
	public void schlagen(String key);
}
