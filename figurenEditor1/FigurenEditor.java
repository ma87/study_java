package figurenEditor1;

public class FigurenEditor {
	private int figurenMaximalzahl=8;
	public int figurenAktuellzahl=3;
	Rechteck[] figuren=new Rechteck[figurenMaximalzahl];
	
	public static void main(String[] args){
		FigurenEditor figEdObject=new FigurenEditor();
		figEdObject.figuren[0]= new Rechteck(13, 15, 7, 8);
		figEdObject.figuren[1]= new Rechteck(27, 23, 6, 5.5);
		figEdObject.figuren[2]= new Rechteck(124, 116, 43, 45);
		
		
		figEdObject.addFigur(23.3, 65, 98, 123);
		figEdObject.addFigur(11, 12, 34, 40);
		figEdObject.addFigur(11, 12, 34, 41);
		figEdObject.addFigur(11, 12, 34, 42);
		figEdObject.addFigur(11, 12, 34, 43);
		figEdObject.addFigur(11, 12, 34, 49);
		
		System.out.println("Alle Elemente:");
		figEdObject.alleFigurenAnzeigen();
		
		System.out.println("Test:");
		figEdObject.test();

		System.out.println("Es sind "+figEdObject.figurenAktuellzahl+"/"+figEdObject.figurenMaximalzahl+" Elemente angelegt...");
	}
	
	public void alleFigurenAnzeigen(){
		for(int i=0;i<figurenAktuellzahl;i++){
			System.out.println("["+i+"] "+figuren[i].toString());
		}
	}
	
	public void addFigur(double zX, double zY, double breite, double hoehe){
		if (figurenAktuellzahl==figurenMaximalzahl) {
			System.out.println("alle "+figurenMaximalzahl+" Speicherplštze sind voll!");
		}
		else{			

			figuren[figurenAktuellzahl++]=new Rechteck(zX, zY, breite, hoehe);
		}
	}
	
	public void test(){
		figurAnzeigen(0);
	}
	
	public void figurAnzeigen(int nummer){
		if (nummer<figurenAktuellzahl){
			System.out.println("["+nummer+"] "+figuren[nummer].toString());		
		}
		else {
			System.out.println("Das Element "+nummer+" existiert nicht!");
		}
	}
}