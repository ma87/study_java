package figurenEditor1;

public class Rechteck {
	private double zentrumX, zentrumY;
	private double breite, hoehe;
	Rechteck(double zX, double zY, double b, double h){
		zentrumX=zX;
		zentrumY=zY;
		breite=b;
		hoehe=h;
	}
	
	public String toString(){
		return "Breite: "+breite+", Hoehe: "+hoehe+", X: "+zentrumX+", Y: "+zentrumY;
	}
	
	public double[] getBoundingBox(){
		return new double[]{zentrumX,zentrumY,breite,hoehe};		
	}
	
	public void verschieben(double translationX, double translationY){
		zentrumX+=translationX;
		zentrumY+=translationY;
	}
	
	public void skalieren(double faktor){
		breite*=faktor;
		hoehe*=faktor;
	}
	
	public double getFlaeche(){
		return breite*hoehe;
	}
}
