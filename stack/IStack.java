package stack;

public interface IStack {

	public Object pop();
	public void push(Object obj);
}
