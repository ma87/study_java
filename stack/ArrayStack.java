package stack;

public class ArrayStack {

	private Object[] list;
	private int idx=0;
	
	public ArrayStack(int size){
		list=new Object[size];
	}
	
	public Object pop() throws ArrayStackEmptyException{
		
		if (idx==0){
			throw new ArrayStackEmptyException();
		} 
		
		return list[--idx];
	}
	
	public void push(Object obj) throws ArrayStackFullException{
		
		if(idx==list.length){
			throw new ArrayStackFullException();
		}
		
		list[idx++]=obj;
	}
}
