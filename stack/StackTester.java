package stack;

public class StackTester {

	public static void main(String[] args){		
		ArrayStack arr = new ArrayStack(5);
		
		System.out.println("Start...");
		
		try {
			arr.push(new Object());
			arr.push(new Object());
			arr.push(new Object());
			arr.push(new Object());
			arr.push(new Object());
			arr.push(new Object()); //Exception
		} catch (ArrayStackFullException e) {
//			System.out.println("FEHLER: Hoppala, das war wohl zu viel!");
			System.out.println(e.getMessage());
		} 
		
		try {
			arr.pop();
			arr.pop();
			arr.pop();
			arr.pop();
			arr.pop();
			arr.pop(); //Exception
		} catch (ArrayStackEmptyException e) {
//			System.out.println("FEHLER: Weniger als nichts geht nicht mehr!");
			System.out.println(e.getMessage());
		}

		System.out.println("Ende");
		
	}

}
