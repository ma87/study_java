package simpleGraph1.graph;

/**
 * Class Graph generates Nodes and Egdes via classes Node and Edge
 * @author Matze
 */
public class Graph {
	/**
	 * Default static values
	 */
	private static final int DEFAULT_NUMBER_OF_NODES=5;
	private static final String DEFAULT_NAME="noname";
	
	/**
	 * Create objects
	 */
	private Node[] a;
	private Edge[] b;
	private int[] nodes;
	
	/**
	 * set name to default
	 */
	private String name=DEFAULT_NAME;
	
	/**
	 * Constructor w/o parameter
	 */
	Graph(){
		this(DEFAULT_NUMBER_OF_NODES);
	}
	/**
	 * Constructor w/ one parameter
	 * @param size number of array-elements/nodes
	 */
	Graph(int size){
		a = new Node[size];
		b = new Edge[size];
		nodes=new int[size];
		for(int i=0;i<size;i++){
			a[i]=new Node(i);
			if (i<(size-1)){
				b[i]=new Edge(i,i+1); //generate Edge with numbers as source and target
				//b[i]=new Edge(a[i],a[i+1]); //generate Edge with references
			}
			else{
				b[i]=new Edge(i,0);
				//b[i]=new Edge(a[1],a[0]);
			}
		}
	}	
	/**
	 * Constructor w/ two parameters
	 * @param size number of array-elements/nodes
	 * @param name the name of the graph
	 */
	Graph(int size, String name){
		this(size);
		setName(name);
		
	}
	
	/**
	 * @return name of the graph
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * set the name of the graph
	 * @param n new name of the graph
	 */
	public void setName(String n){
		name=n;
	}
	
	/**
	 * print out index of each node
	 */	
	public void showNodes(){
		System.out.println("Knotenindex:");
		for (int i=0;i<a.length;i++){
			System.out.println("Index "+a[i].getIndex());
		}	
	}
	
	/**
	 * @return size of node-array/number of nodes
	 */
	public int getNumberOfNodes(){
		return a.length;
	}
	
	/**
	 * print out source and target from all edges
	 */
	public void showPath(){
		System.out.println("Pfad durch Knoten:");
		for (int i=0;i<a.length;i++){
			System.out.println("("+b[i].getSource()+","+b[i].getTarget()+")");
		}
	}
	
	/**
	 * print out source and target from all edges
	 * advanced for object-reference
	 */
	public void showPathN(){
		System.out.println("Pfad durch Knoten:");
		for (int i=0;i<a.length;i++){
			System.out.println("("+b[i].getSourceN()+","+b[i].getTargetN()+")");
		}
	}
	
	/**
	 * add a new edge to the graph
	 * @param src number of source
	 * @param trgt number of target
	 */
	public void addEdge(int src, int trgt){
		//fehlt noch
	}

}
