package simpleGraph1.graph;

/**
 * Class Edge
 * @author Matze
 */
public class Edge {
	/**
	 * Attributes and objects
	 */
	private int s,t;
	private Node sourceNode, targetNode;
	
	/**
	 * Constructor w/ two parameters	
	 * @param source source-number
	 * @param target target-number
	 */
	Edge(int source, int target){
		s=source;
		t=target;
	}
	/**
	 * Constructor w/ two parameters
	 * @param sourceN reference-object to source
	 * @param targetN reference-object to target
	 */
	Edge(Node sourceN, Node targetN){
		sourceNode=sourceN;
		targetNode=targetN;
	}
	
	/**
	 * @return number of source-node
	 */
	public int getSource(){
		return s;
	}
	
	/**
	 * advanced for object-reference
	 * @return number of source-node
	 */
	public int getSourceN(){
		return sourceNode.getIndex();
	}
	
	/**
	 * @return number of target-node
	 */
	public int getTarget(){
		return t;
	}
	
	/**
	 * advanced for object-reference
	 * @return number of target-node
	 */
	public int getTargetN(){
		return targetNode.getIndex();
	}	
}
