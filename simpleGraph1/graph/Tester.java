package simpleGraph1.graph;

/**
 * Class Tester to test any function, generate graph etc.
 * @author Matze
 */
public class Tester {
	public static void main(String[] args) {
		Graph gr=new Graph(11,"Mein Graph"); //create new graph "Mein Graph" with 11 nodes
		//gr.setName("Ich bin ein Graph"); //set name
		gr.showNodes(); //print out all indexes
		gr.showPath(); //print out the path as pair of source and target
		//gr.showPathN();
		System.out.println("Name des Graphs: '"+gr.getName()+"'"); //print out name
		System.out.println("Anzahl an Knoten: "+gr.getNumberOfNodes()); //print out number of nodes
	}
}
