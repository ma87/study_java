package simpleGraph1.graph;

/**
 * Class Node
 * @author Matze
 */
public class Node {
	/**
	 * Attribute
	 */
	private int index;
	
	/**
	 * Constructor w/ one parameter
	 * @param ind index-number
	 */
	Node(int ind){
		index=ind; //set index
	}	
	
	/**
	 * @return index of current node
	 */
	public int getIndex(){
		return index;
	}

}
