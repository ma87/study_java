package landschaft0;

public class Landschaft {
	static int defaultMax=20;
	int maxHoehe=defaultMax-1;
	int[][][] arr;
	
	public Landschaft() {
		arr =new int[defaultMax][defaultMax][];
		for(int x=0;x < defaultMax;x++){
			for(int y=0;y < defaultMax;y++){
				arr[x][y] = new int[Math.max(x, y)];
				for(int z=0; z < Math.max(x, y);z++){
					arr[x][y][z]=z;
				}
			}
		}
	}
	
	static void setDefault(int defaultMax){
		Landschaft.defaultMax=defaultMax;
	}
	
	public int getHoehe(int x, int y){
//		int max=0;
//		int z=0;
//		for(z=0;z < Math.max(x,y);z++){
//			if (arr[x][y][z]>max){
//				max=arr[x][y][z];
//			}
//		}
		return arr[x][y].length;
	}
	
	public String toProfilString(){
		String ret="";
		for(int x=0;x<defaultMax;x++){
			for(int y=0;y<defaultMax;y++){
				ret=ret+"["+getHoehe(x, y)+"] ";
				if (y==maxHoehe){
					ret=ret+"\r\n";
				}
			}
		}
		return ret;
	}

}
