package lotto1.ioConsole;

import java.io.PrintStream;
import java.util.Scanner;

public class IO {
	private static Scanner inputScanner = new Scanner(System.in);
	private static PrintStream output = System.out;
	
	public static void print(String text){
		output.print(text);
	}
	public static void print(int i){
		output.print(i);
	}
	public static void println(String text) {
		print(text + "\n");
	}
	public static void println(int i) {
		print(i + "\n");
	}
	public static int readInt(){
		int i = 0;
		if (inputScanner.hasNextInt()) {
			i = inputScanner.nextInt();
		} else {
			inputScanner.next();
		}
		return i;
	}
	public static String readString(){
		String i = "";
		if (inputScanner.hasNext()) {
			i = inputScanner.next();
		} else {
			inputScanner.next();
		}
		return i;
	}
}
