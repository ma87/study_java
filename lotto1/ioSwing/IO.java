package lotto1.ioSwing;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

public class IO extends JFrame {
	private static final long serialVersionUID = 1L;

	private static JTextField input = 
		new JTextField(
			20
//			/* die Breite des input-Felds wird so bemessen, dass der gr��te
//			 * und kleineste int-Wert sichtbar eingegeben werden kann */
//			Math.max(
//				Integer.toString(Integer.MIN_VALUE).length(),
//				Integer.toString(Integer.MAX_VALUE).length()
//			)
		);
	private static JButton enterButton = new JButton("Enter");
	private static JTextArea output = new JTextArea(10,80);
	private static JScrollPane outputPane = new JScrollPane(output);

	static {
		/* beim Laden der Klasse IO wird eine IO-Instanz erzeugt. Als JFrame
		 * arbeitet dies einer eigenen Thread und lebt daher solange, bis diese
		 * Thread beendet wird, auch wenn hier keine Referenz 
		 * auf die IO-Instanz gehalten wird */
		new IO();
	}

	private IO(){
		/* ---------------- */
		setTitle("IO Swing");
		setLayout(new FlowLayout());
		
		/* input ---------------- */
		add(new JLabel("Input:"));
		add(input);
		input.setFont(new Font("Courier New",Font.PLAIN,14));
		input.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					disableInput();	
				}
			}
		});
		/* enterButton ---------------- */
		add(enterButton);
		/* Der enterButton wird initial zum Klicken gesperrt. */
		enterButton.setEnabled(false); 
		/* Die Klick-Funktionalit�t ist es, die Eingabe abzuschliessen */
		enterButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				disableInput();
			}
		});
		enterButton.setToolTipText(
				"Eingabe abschliessen durch Klicken auf Enter-Button "
				+ "oder durch Dr�cken der <Enter>-Taste im Input-Feld.");

		
		/* output, outputPane ---------------- */
		add(new JLabel("Output:"));
		add(outputPane);
		outputPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		outputPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		output.setFont(new Font("Courier New",Font.PLAIN,14));
		output.setEditable(false);
		
		/* ---------------- */
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setResizable(false);
		setVisible(true);
	}
	public static void println(String text) {
		print(text + "\n");
	}
	public static void println(int i) {
		print(i + "\n");
	}
	public static void print(final String text) {
		/* SwingUtilities.invokeAndWait garantiert, dass die �nderungen am Swing-Gui
		 * auch tats�chlich komplett durchgef�hrt werden, bevor die Thread weitermachen
		 * kann, die print aufruft (z.B. main-Thread).
		 * Stehen die Zeilen der folgenden Methode run einfach so in der Methode print, wird z.B.
		 * in manchen, unvorhersehbaren F�llen, das outputPane nicht ganz nach unten gescrollt.
		 * Dies war tats�chlich ein hartn�ckiger Bug im folgenden Code.
		 */
		try {
			SwingUtilities.invokeAndWait(new Runnable(){
				public void run() {
					/* text zum output zuf�gen */
					output.append(new String(text));
					/* nun outputPane ganz nach unten scrollen:
					 * dazu im VerticalScrollBars die aktuelle Anzeigeposition auf maximum setzen. 
					 */
					JScrollBar scrollBar = outputPane.getVerticalScrollBar();
					scrollBar.setValue(scrollBar.getMaximum());
					 /* Was genauer passiert: scrollBar delegiert die �nderungen an sein "Model", 
					 * dieses widerum benachricht den "View", der sich dann aktualisiert.
					 * Ausf�hrlicher w�re: 
					 * BoundedRangeModel m = outputPane.getVerticalScrollBar().getModel();
					 * m.setValue(m.getMaximum()-m.getExtent());
					 */				
				}		
			});
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	public static void print(final int i) {
		try {
			SwingUtilities.invokeAndWait(new Runnable(){
				public void run() {
					/* i zum output zuf�gen */
					output.append(Integer.toString(i));
					/* nun outputPane ganz nach unten scrollen:
					 * dazu im VerticalScrollBars die aktuelle Anzeigeposition auf maximum setzen. 
					 */
					JScrollBar scrollBar = outputPane.getVerticalScrollBar();
					scrollBar.setValue(scrollBar.getMaximum());
					 /* Was genauer passiert: scrollBar delegiert die �nderungen an sein "Model", 
					 * dieses widerum benachricht den "View", der sich dann aktualisiert.
					 * Ausf�hrlicher w�re: 
					 * BoundedRangeModel m = outputPane.getVerticalScrollBar().getModel();
					 * m.setValue(m.getMaximum()-m.getExtent());
					 */				
				}
			});
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	public static int readInt(){
		input.setToolTipText(
				"Hier int-Wert i eingeben (" 
				+ Integer.MIN_VALUE + " <= i <= " + Integer.MAX_VALUE 
				+ "; illegale Eingaben werden als 0 interpretiert)");
		
		waitForInput();
		/* Nun wird gepr�ft, ob im input-Feld tats�chlich ein int-Wert eingetippt wurde
		 * Dazu wird ein scanner auf den String im input-Textfeld angesetzt
		 * Falls kein int-Wert eingegeben wurde, wird der Wert auf 0 gesetzt */
		Scanner inputScanner = new Scanner(input.getText());
		int i = 0;
		if (inputScanner.hasNextInt()) {
			i = inputScanner.nextInt();
		} else {
			if (inputScanner.hasNext())inputScanner.next();
		}
		/* Das Eingabefeld wird gel�scht. */
		input.setText("");
		return i;
	}
	public static String readString(){
		input.setToolTipText("Hier String eingeben");	
		waitForInput();
		Scanner inputScanner = new Scanner(input.getText());
		inputScanner.useDelimiter("\b");
		String i = "";
		if (inputScanner.hasNext()) {
			i = inputScanner.next();
		} else {
			if (inputScanner.hasNext())inputScanner.next();
		}
		/* Das Eingabefeld wird gel�scht. */
		input.setText("");
		return i;
	}
	private static void waitForInput(){
		synchronized (input) {
			/* Sorgt daf�r, dass die Einf�gemarke im input-Feld steht. */
			input.requestFocus();
			/* input zum Editieren und enterButton zum Klicken freischalten. */
			input.setEditable(true);
			enterButton.setEnabled(true);
			/* Warten bis eine Eingabe gemacht wurde. */
			try {input.wait();} catch (InterruptedException e){}
		}
	}
	private static void disableInput(){
		synchronized (input) {
			/* input zum Editieren und enterButton zum Klicken sperren. */
			input.setToolTipText("");	
			input.setEditable(false);
			enterButton.setEnabled(false);
			/* Thread, die read aufgerufen hat und wartet, nun weitermachen lassen */
			input.notify();		
		}
	}
}
