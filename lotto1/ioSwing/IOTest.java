package lotto1.ioSwing;

import static lotto1.ioSwing.IO.*;

public class IOTest {
	public static void main(String[] args) {
		while (true) {
			print("Bitte int-Zahl eingeben: ");
			println(readInt());
			
			print("Bitte String eingeben: ");
			String text = readString();
			println(text);
		}
	}
}
