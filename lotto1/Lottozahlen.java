package lotto1;

import static lotto1.ioConsole.IO.print;
import static lotto1.ioConsole.IO.println;
import static lotto1.ioConsole.IO.readInt;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

public class Lottozahlen {

	public static void main(String[] args) {
						
		int zahlenBereichMax=49,zahlenAnzahl=6;
		int[] zahlenEingegeben=new int[zahlenAnzahl];
		int[] zahlenZufall=new int[zahlenAnzahl];
		int i=0;
		
		println("__Lottozahlen__");
		println("Du musst "+zahlenAnzahl+" Zahlen von 1 bis "+zahlenBereichMax+" eingeben.\n");
		
		//Eingabe
		while (i<zahlenAnzahl) {			
			zahlenEingegeben[i]=zahlenEinlesen(zahlenAnzahl, zahlenBereichMax, zahlenEingegeben);
			println("Danke für die Eingabe der "+(i+1)+". Zahl");
			i++;
		} 

		//Ausgabe
		Arrays.sort(zahlenEingegeben);
		println("\n\nHier deine eingegebenen Daten:");
		zahlenArrayAusgeben(zahlenEingegeben, true);
				
		//Zufallszahlen erzeugen und ausgeben
		println("Hier die zufälligen Zahlen:");
		zahlenZufall=zahlenGenerieren(zahlenAnzahl, zahlenBereichMax);
		zahlenArrayAusgeben(zahlenZufall, true);
		
		//Anzahl der richtigen ermitteln und ausgeben
		println("\nDu hast "+findeRichtige(zahlenEingegeben, zahlenZufall)+" richtige Zahlen ;)");
	}
	
	/**
	 * @param anzahl Anzahl der einzugebenden Zahlen
	 * @param maxZahl höchste Zahl, die eingegeben werden kann
	 * @param zahlen Array mit den eingegebenen Zahlen
	 * @return Rückgabe der eingegebenen Zahl
	 */
	static int zahlenEinlesen(int anzahl, int maxZahl, int[] zahlen){
		int input=0,i=0;
		
		println("Bitte int-Zahl eingeben:");
		input=readInt();
		
		if (input>maxZahl) {
			println("Zahl zu groß");
			input=zahlenEinlesen(anzahl, maxZahl, zahlen); 
		}
		
		while (i<anzahl){
			if (input==zahlen[i]) { 
				println("Du hast diese Zahl bereits eingegeben.");
				input=zahlenEinlesen(anzahl, maxZahl, zahlen); 
			}		
			i++;
		}
		
		return input;
	}
	
	/**
	 * 
	 * @param anzahl die Anzahl der zu erzeugenden Zahlen
	 * @param maxZahl die maximale zu erzeigende Zahl
	 * @return Array mit Zufallszahlen befüllt
	 */
	static int[] zahlenGenerieren(int anzahl, int maxZahl){
		Random randomizer=new Random();
		int[] zahlenZufall=new int[anzahl];
		int i=0,j=0,k=0;
		boolean doppelt=false;
		
		//doppelte Zahlen verhindern
		while (i<anzahl){
			doppelt=false;
			k=0;
			k=randomizer.nextInt(maxZahl)+1;
			
			while (j<i){
				if (zahlenZufall[i]==k) doppelt=true; 
				j++;
			}			
			if (!doppelt){
				zahlenZufall[i]=k;
				i++;
			}
		}
		
		return zahlenZufall;
	}
	
	/**
	 * 
	 * @param zahlen das auszugebende Array
	 * @param sortieren sollen die Zahlen vor der Ausgabe sortiert werden? 
	 */
	static void zahlenArrayAusgeben(int[] zahlen, boolean sortieren){		
		int i=0;
		DecimalFormat format = new DecimalFormat("00"); //zweistellige Darstellung
		
		//sortieren?
		if (sortieren) Arrays.sort(zahlen);
		while (i<zahlen.length) {
			print("["+format.format(new Double(zahlen[i]))+"] ");
			i++;
		} 
		print("\n");
	}
	
	/**
	 * 
	 * @param z1 erstes Array
	 * @param z2 zweites Array
	 * @return Anzahl der richtigen/übereinstimmenden Zahlen
	 */
	static int findeRichtige(int[] z1, int[] z2){
		int richtig=0,i=0,j=0;
		
		while (i<z1.length){
			while (j<z2.length){
				if (z1[i]==z2[j]) richtig++;
				j++;
			}
			j=0;
			i++;
		}
		
		return richtig;
	}
}
