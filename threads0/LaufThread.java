package threads0;

public class LaufThread extends Thread{
	private boolean left=true;
	
	public void run(){
		if (left) {
			System.out.println("links"); 
		}
		else {
			System.out.println("rechts"); 
		}
		left=!left;
		
		try {
			Thread.sleep((long) 1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
