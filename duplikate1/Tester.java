package duplikate1;

public class Tester {
	Komponist[] komp=new Komponist[4];

	public static void main(String[] args) {		
		Tester test=new Tester();
		test.doTest();
	}
	
	public void doTest(){
		Komponist k1=new Komponist("Hans", "Dorn", 1804, 1845);
		Komponist k2=new Komponist("Martin", "�bel", 1835, 1873);
		Komponist k3=new Komponist("Hans", "Dorn", 1804, 1845);
		Komponist k4=new Komponist("Manfred", "Muster", 1844, 1912);
		Komponist k5=new Komponist("Martin", "�bel", 1835, 1873);
		Komponist k6=new Komponist("Willi", "Manana", 1844, 1919);
		DuplikateChecker dupl=new DuplikateChecker();
		String output;
		
		if (dupl.duplikat(k3, k2)) { output="ein"; } else { output="kein"; }
		System.out.println("Die Komponisten '"+k2.getNachname()+"' und '"+k3.getNachname()+"' sind "+output+" Duplikat");
		
		if (dupl.duplikat(k1, k4)) { output="ein"; } else { output="kein"; }
		System.out.println("Die Komponisten '"+k1.getNachname()+"' und '"+k4.getNachname()+"' sind "+output+" Duplikat");	
		
		if (dupl.duplikat(k2, k5)) { output="ein"; } else { output="kein"; }
		System.out.println("Die Komponisten '"+k2.getNachname()+"' und '"+k5.getNachname()+"' sind "+output+" Duplikat");
		
		if (dupl.duplikat(k3, k6)) { output="ein"; } else { output="kein"; }
		System.out.println("Die Komponisten '"+k3.getNachname()+"' und '"+k6.getNachname()+"' sind "+output+" Duplikat");
	}
}
