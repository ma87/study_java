package duplikate1;

public class Komponist {
	private String vorname, nachname;
	private int geburtsjahr, sterbejahr;
	Komponist(String vorname, String nachname, int geburtsjahr, int sterbejahr){
		this.vorname=vorname;
		this.nachname=nachname;
		this.geburtsjahr=geburtsjahr;
		this.sterbejahr=sterbejahr;
	}
	
	public boolean equals (Object komponist){
		Komponist k=(Komponist)komponist;

		if (vorname.equals(k.vorname) && nachname.equals(k.nachname) && geburtsjahr == k.geburtsjahr && sterbejahr == k.sterbejahr) return true;		
		return false;
	}

	//Getter + Setter
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public int getGeburtsjahr() {
		return geburtsjahr;
	}

	public void setGeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}

	public int getSterbejahr() {
		return sterbejahr;
	}

	public void setSterbejahr(int sterbejahr) {
		this.sterbejahr = sterbejahr;
	}
}
