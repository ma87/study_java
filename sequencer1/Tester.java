package sequencer1;

public class Tester {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Sequencer seq=new Sequencer();
		//seq.play();
		Tester myTest=new Tester();
		myTest.test();
	}
	
	public void test(){
		Sequencer seq=new Sequencer();
		seq.record();
		seq.mute(2);
		seq.play();
		seq.mute(1);
		seq.mute(2);
		seq.mute(3);
		seq.mute(4);
		seq.mute(4);
	}
}
