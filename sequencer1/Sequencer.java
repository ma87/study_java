package sequencer1;

public class Sequencer {
	//private AudioTrack a1,a2;
	private AudioTrack[] audio=new AudioTrack[2];
	private MidiTrack[] midi=new MidiTrack[2];
	
	public Sequencer() {
		//a1=new AudioTrack("Lalelu");
		//a2=new AudioTrack("ein anderer Titel");
		audio[0]=new AudioTrack("Ummz ummz ummz");
		audio[1]=new AudioTrack("Badabu");
		midi[0]=new MidiTrack();
		midi[1]=new MidiTrack();		
	}
	
	public void record(){
		audio[0].record();
		audio[1].record();
		midi[0].record();
		midi[1].record();
	}
	
	public void mute(int track){
		switch (track) {
		case 4:
			midi[1].mute();
			break;
		case 3:
			midi[0].mute();
			break;
		case 2:
			audio[1].mute();
			break;
		case 1:
			audio[0].mute();
			break;

		default:
			audio[0].mute();
			break;
		}
	}
	
	public void play(){
		//a1.play();
		//a2.play();
		audio[0].play();
		audio[1].play();
		midi[0].play();
		midi[1].play();
	}
}
