package sequencer1;

public class AudioTrack {
	private String name;
	private boolean muted;
	
	public AudioTrack(String name){
		this.name=name;
	}
	
	public void record(){
		System.out.println("recording Audiotrack "+name);
	}
	
	public void mute(){
		muted=!muted;
		if (!muted) System.out.print("un");
		System.out.println("muted Audiotrack "+name);
	}
	
	public void play(){
		System.out.println("playing Audiotrack "+name);
	}
}
