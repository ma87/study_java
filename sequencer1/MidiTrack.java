package sequencer1;

public class MidiTrack {
	private boolean muted;
	
	public MidiTrack(){
		
	}
	
	public void record(){
		System.out.println("recording Miditrack");
	}
	
	public void mute(){
		muted=!muted;
		if (!muted) System.out.print("un");
		System.out.println("muted Miditrack");
	}
	
	public void play(){
		System.out.println("playing Miditrack");
	}
}
